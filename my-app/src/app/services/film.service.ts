import { Injectable } from '@angular/core';
import { Mock } from 'protractor/built/driverProviders';
import { BehaviorSubject } from 'rxjs';

interface Film {
	path_to_image: string;
	name: string;
	id: number;
	year: string;
	information: string;
	genre: string;
}

const MockFilms: Array<Film> = [
	{
		name: 'Минари',
		year: '2020',
		id: 1,
		information: "1980-е годы. Семья корейских иммигрантов с двумя детьми переезжает из Калифорнии в Арканзас." +
			" Глава семейства купил тут участок земли и собирается заняться фермерством," +
			" но жизнь постоянно подкидывает молодой семье новые трудности.",
		path_to_image: "../../../assets/minary.svg",
		genre: "Драма"
	},
	{
		name: 'Солнцестояние',
		year: '2019',
		id: 2,
		information: "День летнего солнцестояния – древний праздник, который во всех культурах окутан мистическим" +
			" ореолом. В отрезанном от цивилизованного мира шведском поселении в этот день проводятся уникальные" +
			" обряды с многовековой традицией. Именно туда отправляется группа молодых американских" +
			" студентов-антропологов, прихватив с собой девушку одного из них. Однако вскоре после прибытия друзья" +
			" выясняют, что местные обряды далеко не безобидны.",
		path_to_image: "../../../assets/solstice.svg",
		genre: "Драма, Триллер, Фильм ужасов"
	},
	{
		name: 'Манк',
		year: '2020',
		id: 3,
		information: "1940 год. Нью-йоркский острослов Герман Манкевич, когда-то приехавший покорять Голливуд" +
			" и в целом преуспевший, с переломами после автоаварии уединяется в домике посреди пустыни Мохаве." +
			" Он помещен в заточение продюсером Джоном Хаусменом и молодым режиссером Орсоном Уэллсом:" +
			" Манк — так называют его друзья — должен всего за три месяца написать для Уэллса сценарий." +
			" А лучше бы за два. Манк вспоминает последние годы, проведённые в Голливуде, и переносит опыт" +
			" личного общения с воротилами бизнеса в сценарий.",
		path_to_image: "../../../assets/mank.svg",
		genre: "Драма, Кинокомедия"
	},
	{
		name: 'Девушка, подающая надежды',
		year: '2020',
		id: 4,
		information: "Когда-то Кассандра училась в университете и подавала большие надежды, но теперь живёт" +
			" с родителями и работает в кофейне. Девушка скрывает от близких, что по вечерам посещает бары" +
			" и другие увеселительные заведения, где, притворяясь сильно пьяной, ведёт охоту на падких" +
			" до беззащитных женщин мужчин. Вымещая злость на противоположном поле, таким образом Кассандра" +
			" мстит за события студенческих лет.",
		path_to_image: "../../../assets/girl.svg",
		genre: "Драма, Кинокомедия, Триллер"
	},
	{
		name: 'Земля кочевников',
		year: '2020',
		id: 5,
		information: "После закрытия завода по производству гипсокартона в январе 2011 года 60-летняя женщина Ферн" +
			" остаётся без работы и средств к существованию. Погрузив свой небогатый скарб в фургон," +
			" Ферн становится современным кочевником — одной из людей, путешествующих из штата в штат в поисках" +
			" сезонного заработка.",
		path_to_image: "../../../assets/land.svg",
		genre: "Драма"
	},
	{
		name: 'Отец',
		year: '2020',
		id: 6,
		information: "Энтони уже далеко не молод и живет один в лондонской квартире. Он считает, что прекрасно" +
			" справляется со всем сам, и помощь ему не нужна, но его дочь Энн противоположного мнения." +
			" Она собралась переезжать в Париж и хочет найти отцу сиделку. Энтони упирается всеми возможными" +
			" способами, но в его квартире начинают происходить странности — в гостиной оказывается посторонний" +
			" мужчина, дочь вдруг меняет внешность, и куда-то постоянно пропадают любимые часы.",
		path_to_image: "../../../assets/father.svg",
		genre: "Драма"
	},
	{
		name: 'Еще по одной',
		year: '2020',
		id: 7,
		information: "В ресторане собираются учитель истории, психологии, музыки и физрук, чтобы отметить" +
			" 40-летие одного из них. И решают проверить научную теорию о том, что c самого рождения человек" +
			" страдает от нехватки алкоголя в крови, а чтобы стать по-настоящему счастливым, нужно быть немного" +
			" нетрезвым. Друзья договариваются наблюдать, как возлияния скажутся на их работе и личной жизни," +
			" и устанавливают правила: не пить вечером и по выходным. Казалось бы, что может пойти не так?",
		path_to_image: "../../../assets/one_more.svg",
		genre: "Драма, Кинокомедия"
	},
	{
		name: 'Душа',
		year: '2020',
		id: 8,
		information: "Уже немолодой школьный учитель музыки Джо Гарднер всю жизнь мечтал выступать на сцене" +
			" в составе джазового ансамбля. Однажды он успешно проходит прослушивание у легендарной саксофонистки" +
			" и, возвращаясь домой вне себя от счастья, падает в люк и умирает. Теперь у Джо одна дорога" +
			" — в Великое После, но он сбегает с идущего в вечность эскалатора и случайно попадает в Великое До." +
			" Тут новенькие души обретают себя, и у будущих людей зарождаются увлечения, мечты и интересы." +
			" Джо становится наставником упрямой души 22, которая уже много веков не может найти свою искру" +
			" и отправиться на Землю.",
		path_to_image: "../../../assets/soul.svg",
		genre: "Драма, Фэнтези, Кинокомедия"
	},
	{
		name: 'Годзилла',
		year: '2014',
		id: 9,
		information: "Человечество случайно разбудило гигантское древнее существо, что повлекло" +
			" за собой ужасающие последствия.",
		path_to_image: "../../../assets/godzilla.svg",
		genre: "Боевик, Триллер"
	},
	{
		name: 'Довод',
		year: '2020',
		id: 10,
		information: "После теракта в киевском оперном театре агент ЦРУ объединяется с британской разведкой," +
			" чтобы противостоять русскому олигарху, который сколотил состояние на торговле оружием." +
			" Для этого агенты используют инверсию времени — технологию будущего, позволяющую времени идти вспять.",
		path_to_image: "../../../assets/argument.svg",
		genre: "Фантастика, Боевик, Триллер"
	},
	{
		name: 'Реинкарнация',
		year: '2018',
		id: 11,
		information: "После смерти матери в ранее спокойном доме Энни начинает твориться нечто необъяснимое." +
			" Теперь под угрозой жизнь её близких, и с каждым днём открываются новые ужасающие секреты семьи.",
		path_to_image: "../../../assets/reincarnation.svg",
		genre: "Фильм ужасов, Драма, Триллер"
	},
	{
		name: 'Сияние',
		year: '1980',
		id: 12,
		information: "Джек Торренс с женой и сыном приезжает в элегантный отдалённый отель," +
			" чтобы работать смотрителем во время мертвого сезона. Торренс здесь раньше никогда не бывал." +
			" Или это не совсем так? Ответ лежит во мраке, сотканном из преступного кошмара.",
		path_to_image: "../../../assets/shine.svg",
		genre: "Филь ужасов, Детектив"
	},
	{
		name: 'Никто',
		year: '2021',
		id: 13,
		information: "Непримечательный и незаметный семьянин Хатч живёт скучной жизнью обычного аудитора," +
			" пока однажды в его дом не вламываются грабители. И это бы сошло им с рук, если бы они не забрали" +
			" браслетик его маленькой дочки. Не в силах это терпеть, Хатч отправляется на поиски наглецов," +
			" а на обратном пути ввязывается в драку с пьяными хулиганами, пристававшими к девушке в общественном" +
			" транспорте. От души помахав кулаками, наш аудитор отправляет дебоширов в больницу, но оказывается," +
			" что один из пострадавших — брат влиятельного русского бандита. И он теперь жаждет мести.",
		path_to_image: "../../../assets/no_one.svg",
		genre: "Драма, Боевик, Триллер"
	},
	{
		name: 'Джентльмены',
		year: '2019',
		id: 14,
		information: "Один ушлый американец ещё со студенческих лет приторговывал наркотиками," +
			" а теперь придумал схему нелегального обогащения с использованием поместий обедневшей английской" +
			" аристократии и очень неплохо на этом разбогател. Другой пронырливый журналист приходит к Рэю," +
			" правой руке американца, и предлагает тому купить киносценарий, в котором подробно описаны" +
			" преступления его босса при участии других представителей лондонского криминального мира" +
			" — партнёра-еврея, китайской диаспоры, чернокожих спортсменов и даже русского олигарха.",
		path_to_image: "../../../assets/gentlemen.svg",
		genre: "Кинокомедия, Боевик"
	},
	{
		name: 'Однажды... в Голливуде',
		year: '2019',
		id: 15,
		information: "1969 год, золотой век Голливуда уже закончился. Известный актёр Рик Далтон и его дублер" +
			" Клифф Бут пытаются найти свое место в стремительно меняющемся мире киноиндустрии.",
		path_to_image: "../../../assets/once.svg",
		genre: "Кинокомедия, Драма"
	},
	{
		name: 'Джокер',
		year: '2019',
		id: 16,
		information: "Готэм, начало 1980-х годов. Комик Артур Флек живет с больной матерью," +
			" которая с детства учит его «ходить с улыбкой». Пытаясь нести в мир хорошее и дарить людям радость," +
			" Артур сталкивается с человеческой жестокостью и постепенно приходит к выводу," +
			" что этот мир получит от него не добрую улыбку, а ухмылку злодея Джокера.",
		path_to_image: "../../../assets/joker.svg",
		genre: "Драма, Триллер"
	},
	{
		name: 'Три билборда на границе Эббинга...',
		year: '2019',
		id: 17,
		information: "Спустя несколько месяцев после убийства дочери Милдред Хейс преступники так и не найдены." +
			" Отчаявшаяся женщина решается на смелый шаг - арендует на въезде в город три билборда и оставляет" +
			" на них послание главе полиции Уильяму Уиллоуби. Когда в ситуацию оказывается втянут ещё" +
			" и заместитель шерифа Диксон, инфантильный маменькин сынок со склонностью к насилию," +
			" борьба между Милдред и властями города только усугубляется.",
		path_to_image: "../../../assets/billboards.svg",
		genre: "Драма, Кинокомедия"
	},
	{
		name: 'Паразиты',
		year: '2019',
		id: 18,
		information: "Обычное корейское семейство Кимов жизнь не балует. Приходится жить в сыром грязном" +
			" полуподвале, воровать интернет у соседей и перебиваться случайными подработками." +
			" Однажды друг сына семейства, уезжая на стажировку за границу, предлагает тому заменить" +
			" его и поработать репетитором у старшеклассницы в богатой семье Пак. Подделав диплом о высшем" +
			" образовании, парень отправляется в шикарный дизайнерский особняк и производит на хозяйку дома" +
			" хорошее впечатление. Тут же ему в голову приходит необычный план по трудоустройству сестры.",
		path_to_image: "../../../assets/parasites.svg",
		genre: "Драма, Кинокомедия, Триллер"
	},
	{
		name: 'Фильм не найден',
		year: '',
		id: 19,
		information: "",
		path_to_image: "../../../assets/not_found.svg",
		genre: ""
	}
];

@Injectable({
	providedIn: 'root'
})
export class FilmService {

	films$: BehaviorSubject<Array<Film>> = new BehaviorSubject<Array<Film>>([]);

	searchFilm(filmName: string): void {
		if (filmName.length < 1) {
			this.films$.next([]);
			return;
		}

		const value = MockFilms.filter(film => film.name.startsWith(filmName));

		this.films$.next(value);
	}

	constructor() {
	}
}
