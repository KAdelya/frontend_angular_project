import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	host: {
		'[class.home]': 'true'
	}
})
export class HomeComponent {
	form: FormGroup;
	modalIsOpen: boolean = false;

	constructor(private fb: FormBuilder) {
		this.form = this.fb.group({
			toggle: [true],
		});

		this.form.valueChanges.subscribe(res => {
			console.log(this.form);
		});
	}

	formatValue(arr: any[], id): any {
		return arr.find(item => item.id === id);
	}

	openModal(): void {
		this.modalIsOpen = true;
	}
}
