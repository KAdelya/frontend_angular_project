import { OverlayModule } from '@angular/cdk/overlay';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GenreSearchComponent } from './components/genre-search/genre-search.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { UsersComponent } from './pages/users/users.component';
import { UserComponent } from './pages/users/user/user.component';
import { ToggleComponent } from './components/toggle/toggle.component';
import {FormatValuePipe} from "./pipes/format-value.pipe";


@NgModule({
	schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		UsersComponent,
		GenreSearchComponent,
		UserComponent,
		ToggleComponent,
		FormatValuePipe
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		OverlayModule
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
