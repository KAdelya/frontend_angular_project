import { ChangeDetectionStrategy, Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from "@angular/forms";

interface Option<T = string> {
	label: string;
	value: T;
}

export const Genres: Array<Option> = [
	{
		label: 'Кинокомедия',
		value: 'comedy'
	}, {
		label: 'Боевик',
		value: 'action'
	}, {
		label: 'Фильм Нуар',
		value: 'noir'
	}, {
		label: 'Фэнтези',
		value: 'fantasy'
	}, {
		label: 'Драма',
		value: 'drama'
	}, {
		label: 'Триллер',
		value: 'thriller'
	}, {
		label: 'Фильм ужасов',
		value: 'horror'
	}, {
		label: 'Фантастика',
		value: 'phantasy'
	}, {
		label: 'Детектив',
		value: 'detective'
	}
];

@Component({
	selector: 'app-genre-search',
	templateUrl: './genre-search.component.html',
	styleUrls: ['./genre-search.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => GenreSearchComponent),
			multi: true
		}
	]
})
export class GenreSearchComponent implements ControlValueAccessor {
	@Input() placeholder: string = 'Жанр';

	inputSearch: FormControl = new FormControl('');

	genres: Array<Option> = [];

	overlayIsOpen: boolean = false;

	selected: Array<Option> = [];
	arr_for_out = [];

	onChangeCallback = (v) => {
	};
	onTouchedCallback = () => {
	};

	constructor() {
		this.inputSearch.valueChanges.subscribe(value => {
			if (!value || value.length < 1) {
				this.genres = [];
				this.overlayIsOpen = false;
				return;
			}

			this.genres = Genres.filter(item => item.label.startsWith(value));
			this.overlayIsOpen = true;
		});
	}

	writeValue(value: Array<Option>): void {
		if (value !== this.selected) {
			this.selected = value;
		}
	}

	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}


	removeGenre(genre: Option): void {
		this.selected = this.selected.filter(item => item.value !== genre.value);
	}

	selectGenre(genre: Option): void {
		this.selected.push(genre);
		this.arr_for_out.push(genre);
		this.overlayIsOpen = false;
		this.inputSearch.reset();
		this.onChangeCallback(this.selected);
		this.onTouchedCallback();
		// this.inputSearch.patchValue(this.selected.map(item => item.label + ', '));
	}

	close(): void {
		this.overlayIsOpen = false;
	}

}
