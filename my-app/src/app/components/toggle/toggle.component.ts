import { ChangeDetectionStrategy, Component, forwardRef} from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { delay, distinctUntilChanged } from 'rxjs/operators';
import { FilmService } from '../../services/film.service';
@Component({
	selector: 'app-toggle',
	templateUrl: './toggle.component.html',
	styleUrls: ['./toggle.component.sass'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => ToggleComponent),
			multi: true
		}
	]
})
export class ToggleComponent implements ControlValueAccessor {
	form: FormGroup;
	modalIsOpen: boolean = false;
	films$ = this.filmService.films$;
	title: string;
	id: number;
	name: string;
	information: string;
	genre: string;
	names: any;
	innerValue: boolean = true;
	inputValue: string;
	year: string;
	formArr = [];
	public uname: string;

	onChangeCallback = (v) => {};
	onTouchedCallback = () => {};
	registerOnChange(fn: any): void {
		this.onChangeCallback = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouchedCallback = fn;
	}

	writeValue(value: boolean): void {
		if (value !== this.innerValue) {
			this.innerValue = value;
		}
	}

	toggle(value: boolean): void {
		if (value !== this.innerValue) {
			this.innerValue = value;
			this.onChangeCallback(value);
			this.onTouchedCallback();
		}
	}
	constructor(
		private fb: FormBuilder,
		private filmService: FilmService
	) {
		this.form = this.fb.group({
			searchInput: []
		});

		this.form.get('searchInput').valueChanges
			.pipe(
				delay(700),
				distinctUntilChanged(),
			)
			.subscribe(res => {
				this.filmService.searchFilm(res);
			});
	}

	formatValue(arr: any[], id): any {
		return arr.find(item => item.id === id);
	}

	openModal(value: boolean, id, name, year, information, genre): any {
		if (value !== this.modalIsOpen) {
			this.modalIsOpen = value;
			this.id = id;
			this.name = name;
			this.information = information;
			this.genre = genre;
			this.year = year;
			this.onChangeCallback(value);
			this.onTouchedCallback();
		}
	}
	check(): any {
		if (this.modalIsOpen === true) {
			this.modalIsOpen = false;
		}
	}
	removeElement(inx: number): any {
		(this.form.get('form') as FormArray).removeAt(inx);
	}
	save(): any {
		this.formArr.push(this.uname)
	}
}
